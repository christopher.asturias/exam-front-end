import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
// import axios from "axios";
import VueResource from 'vue-resource';

// Vue.prototype.$axios = window.axios;
Vue.use(VueResource);
Vue.config.productionTip = false

window.Event =  new Vue;

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')


