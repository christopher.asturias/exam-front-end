import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
import Register from '@/components/Register'
import User from '@/components/User'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/',
    name: 'user',
    component: User
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
